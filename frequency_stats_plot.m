%% clear stuff...
clear all global
close all
clc

%% init obob_ownft...
addpath('obob_ownft/')
obob_init_ft;

%% add functions...
addpath('functions/')

%% set vars...
preproc_dir = 'data/powerspectra/';

all_subjects = dir(fullfile(preproc_dir, '*'));
all_subjects = {all_subjects(3:end).name};

% paths to save...
path2save = 'data/data4py';
if ~exist(path2save, 'dir')
    mkdir(path2save);
end %if

%% Load data and do freqanalysis...
for idx_sub = 1:length(all_subjects)
    
    cur_subject = all_subjects{idx_sub};
    
    % load data...
    load(fullfile(preproc_dir, cur_subject)); %'psd_ci'
        
    % get the relevant parts for decoding...
    powspctrm = squeeze(psd_ci.powspctrm);
    trlinfo = psd_ci.trialinfo(:,1);
    freqs = psd_ci.freq;
    
    % save for decoding in python...
    save(fullfile(path2save, cur_subject), 'powspctrm', 'trlinfo', 'freqs')
    
    % select audio / visual trials...
    trials = [];
    trials.audio = psd_ci.trialinfo(:, 1) == 1;
    trials.visual = psd_ci.trialinfo(:, 1) == 0;
    
    cfg = [];
    cfg.avgoverrpt = 'yes';
    cfg.trials = trials.audio;
    data_spectrum.audio{idx_sub} = ft_selectdata(cfg, psd_ci);
    
    cfg.trials = trials.visual;
    data_spectrum.visual{idx_sub} = ft_selectdata(cfg, psd_ci);
       
end %for

%% grand-average spectra...
ga_spectra = [];

cfg = [];
ga_spectra.audio = ft_freqgrandaverage(cfg, data_spectrum.audio{:});
ga_spectra.visual = ft_freqgrandaverage(cfg, data_spectrum.visual{:});

%% smooth spectra...
psd_smooth = [];

for idx_sub = 1:length(all_subjects)
    
    psd_smooth.audio{idx_sub} = data_spectrum.audio{idx_sub};
    psd_smooth.audio{idx_sub}.powspctrm = smooth(psd_smooth.audio{idx_sub}.powspctrm)';
    psd_smooth.visual{idx_sub} = data_spectrum.visual{idx_sub};
    psd_smooth.visual{idx_sub}.powspctrm = smooth(psd_smooth.visual{idx_sub}.powspctrm)';
    
end %for

% grand average...
ga_smooth.audio = ft_freqgrandaverage([], psd_smooth.audio{:});
ga_smooth.visual = ft_freqgrandaverage([], psd_smooth.visual{:});

% relative change...
ga_relchange = ga_smooth.audio;
ga_relchange.powspctrm = log10(ga_smooth.audio.powspctrm ./ ga_smooth.visual.powspctrm) * 100;


%% calculate Cousineau corrected error bars...
data_for_cous = zeros(length(all_subjects), 2, length(psd_smooth.audio{1}.powspctrm));
se_tmp = zeros(length(all_subjects), 2);

for idx_sub = 1:length(all_subjects)
    
    for idx_freq = 1:length(psd_smooth.audio{idx_sub}.powspctrm)
        data_for_cous(idx_sub, 1, idx_freq) = psd_smooth.audio{idx_sub}.powspctrm(idx_freq);
        data_for_cous(idx_sub, 2, idx_freq) = psd_smooth.visual{idx_sub}.powspctrm(idx_freq);
        se_tmp(idx_freq,:) = within_subject_error(data_for_cous(:,:,idx_freq));
    end % for
    
end % for

se_cous_audio = se_tmp(:,1);
se_cous_visual = se_tmp(:,2);

%% Plot Fig.1A...
figure(1)

% relative change...
h1 = subplot(2,1,1);
%ax = axes();
area(ga_relchange.freq, ga_relchange.powspctrm, 'EdgeColor', 'none', 'FaceColor', [0.5 0.5 0.5]);
%axis square
xlim([5 24]);
%xticks(4:3:25);
ylim([-2 2]);
yticks([-2 0 2]);
%xlabel('\it Frequency (Hz)');
ylabel('\it Relative change (%)');
box off
set(h1, 'Position', [0.1300 0.8036 0.7750 0.1510])
set(gca,'FontSize', 10, 'FontName', 'WeblySleek UI Light')
set(gca,'XTick', [])
title('Prestimulus power')

% Plot CI spectra with Cousineau corrected error bars...
h2 = subplot(2,1,2);
boundedline(ga_spectra.audio.freq, ...
    ga_spectra.audio.powspctrm(1,:), se_cous_audio, ...
    'cmap', [0.7, 0.15, 0.7], 'alpha', 'transparency', 0.1);

hold on
p1 = plot(ga_spectra.audio.freq, ga_spectra.audio.powspctrm(1,:),'LineWidth', 3, 'Color', [0.7, 0.15, 0.7]);
p2 = plot(ga_spectra.visual.freq, ga_spectra.visual.powspctrm(1,:),'LineWidth', 3, 'Color', [0, 0.5, 0.45]);

boundedline(ga_spectra.visual.freq, ...
    ga_spectra.visual.powspctrm(1,:), se_cous_visual, ...
    'cmap', [0, 0.5, 0.45], 'alpha', 'transparency', 0.1);

xlim([5 24]);
set(gca,'FontSize', 10, 'FontName', 'WeblySleek UI Light')
set(h2, 'Position', [0.1300 0.1100 0.7750 0.6412])
xlabel('\it Frequency (Hz)');
ylabel('\it Power spectral density (V ^{2})');
legend([p1(1),p2(1)], '\it Attend audio', '\it Attend visual');

%% Do cluster-based permutation test...
% dependent t-tests, within subjects...

% theta...
cfg = [];
cfg.method = 'montecarlo';
cfg.statistic = 'ft_statfun_depsamplesT';
cfg.correctm = 'cluster';
cfg.clusterstatistic = 'maxsize';
cfg.tail = 1;
cfg.correcttail = 'prob';
cfg.numrandomization = 10000;
cfg.frequency = [5 8]; 
cfg.avgoverfreq = 'yes';

cfg.design = [];
cfg.design(1, :) = [ones(length(data_spectrum.audio), 1); ones(length(data_spectrum.audio), 1) * 2];
cfg.design(2, :) = repmat(1:length(data_spectrum.audio), 1, 2);
cfg.ivar = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar = 2; % the 2nd row in cfg.design contains the subject number

stats_cluster_theta = ft_freqstatistics(cfg, data_spectrum.audio{:}, data_spectrum.visual{:});

stats_cluster_theta.prob(stats_cluster_theta.prob < 1)
stats_cluster_theta.freq(stats_cluster_theta.prob < 1)
stats_cluster_theta.mask(stats_cluster_theta.prob < 1)

% alpha...
cfg.frequency = [9 13];
stats_cluster_alpha = ft_freqstatistics(cfg, data_spectrum.audio{:}, data_spectrum.visual{:});

stats_cluster_alpha.prob(stats_cluster_alpha.prob < 1)
stats_cluster_alpha.freq(stats_cluster_alpha.prob < 1)
stats_cluster_alpha.mask(stats_cluster_alpha.prob < 1)

% beta...
cfg.frequency = [14 24];
stats_cluster_beta = ft_freqstatistics(cfg, data_spectrum.audio{:}, data_spectrum.visual{:});

stats_cluster_beta.prob(stats_cluster_beta.prob < 1)
stats_cluster_beta.freq(stats_cluster_beta.prob < 1)
stats_cluster_beta.mask(stats_cluster_beta.prob < 1)

%% Calculate cohens d...

% theta...
cfg = [];
cfg.method = 'analytic';
cfg.statistic = 'cohensd';
cfg.frequency = [5 8]; % theta: [5 8], alpha : [9 13]
cfg.avgoverfreq = 'yes';

cfg.design = [];
cfg.design(1, :) = [ones(length(data_spectrum.audio), 1); ones(length(data_spectrum.audio), 1) * 2];
cfg.design(2, :) = repmat(1:length(data_spectrum.audio), 1, 2);
cfg.ivar = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar = 2; % the 2nd row in cfg.design contains the subject number

stats_cohensd_theta = ft_freqstatistics(cfg, data_spectrum.audio{:}, data_spectrum.visual{:});

% alpha...
cfg.frequency = [9 13];
stats_cohensd_alpha = ft_freqstatistics(cfg, data_spectrum.audio{:}, data_spectrum.visual{:});

% beta...
cfg.frequency = [14 24];
stats_cohensd_beta = ft_freqstatistics(cfg, data_spectrum.audio{:}, data_spectrum.visual{:});

%% print results...
fprintf('\n\n\ntheta: cluster: p = %.5f ; cohens d: %0.02f \n', stats_cluster_theta.prob, stats_cohensd_theta.cohensd);
fprintf('alpha: cluster: p = %.5f ; cohens d: %0.02f \n', stats_cluster_alpha.prob, stats_cohensd_alpha.cohensd);
fprintf('beta: cluster: p = %.5f ; cohens d: %0.02f \n', stats_cluster_beta.prob, stats_cohensd_beta.cohensd);

%% get individual theta / alpha power...
for idx_sub = 1:length(all_subjects)
    
    % theta...
    cfg = [];
    cfg.frequency = [5 8];
    cfg.avgoverfreq = 'yes';
    
    cur_psd = ft_selectdata(cfg, data_spectrum.audio{idx_sub});
    psd_theta.audio(1,idx_sub) = cur_psd.powspctrm;
    cur_psd = ft_selectdata(cfg, data_spectrum.visual{idx_sub});
    psd_theta.visual(1,idx_sub) = cur_psd.powspctrm;
    
    % alpha...
    cfg.frequency = [9 13];
    cur_psd = ft_selectdata(cfg, data_spectrum.audio{idx_sub});
    psd_alpha.audio(1,idx_sub) = cur_psd.powspctrm;
    cur_psd = ft_selectdata(cfg, data_spectrum.visual{idx_sub});
    psd_alpha.visual(1,idx_sub) = cur_psd.powspctrm;
    
end %for

%% get mean theta / alpha power...
psd_theta_mean.audio = mean(psd_theta.audio);
psd_theta_mean.visual = mean(psd_theta.visual);
psd_alpha_mean.audio = mean(psd_alpha.audio);
psd_alpha_mean.visual = mean(psd_alpha.visual);

%% plot Fig.2B...
figure(2)
% theta...
subplot(1,2,1)
plot([0.5 1.5], [psd_theta.audio' psd_theta.visual'], 'Color', [0.7 0.7 0.7], 'LineWidth', 1)
hold on
plot(0.5, psd_theta.audio, '.', 'MarkerSize', 20, 'Color', [0.7, 0.15, 0.7])
plot(0.35, psd_theta_mean.audio, '.', 'MarkerSize', 30, 'Color', [0 0 0]);
plot(1.5, psd_theta.visual, '.b', 'MarkerSize', 20, 'Color', [0, 0.5, 0.45])
plot(1.65, psd_theta_mean.visual, '.', 'MarkerSize', 30, 'Color', [0 0 0]);
xlim([0 2])
ylim([5.0*1e-10 9.8*1e-10])
xticks([0.5 1.5])
xticklabels({'Auditory', 'Visual'})
ylabel('\it Power spectral density (V ^{2})');
title('\it Theta (5-8 Hz)')

% alpha...
subplot(1,2,2)
plot([0.5 1.5], [psd_alpha.audio' psd_alpha.visual'], 'Color', [0.7 0.7 0.7], 'LineWidth', 1)
hold on
plot(0.5, psd_alpha.audio, '.', 'MarkerSize', 20, 'Color', [0.7, 0.15, 0.7])
plot(0.35, psd_alpha_mean.audio, '.', 'MarkerSize', 30, 'Color', [0 0 0]);
plot(1.5, psd_alpha.visual, '.b', 'MarkerSize', 20, 'Color', [0, 0.5, 0.45])
plot(1.65, psd_alpha_mean.visual, '.', 'MarkerSize', 30, 'Color', [0 0 0]);
xlim([0 2])
ylim([5.0*1e-10 10*1e-10])
xticks([0.5 1.5])
xticklabels({'Auditory', 'Visual'})
title('\it Alpha (9-13 Hz)')

set(gca,'FontSize', 10, 'FontName', 'WeblySleek UI Light')
sgtitle('\it Auditory vs. Visual')