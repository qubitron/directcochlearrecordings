## import...
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import pingouin as pg

from os import listdir
from os.path import isfile, join
from pymatreader import read_mat
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import permutation_test_score

## set vars...
data_path = '/mnt/obob/staff/qgehmacher/git/directcochlearrecordings/data/data4py/'
all_files = [f for f in listdir(data_path) if isfile(join(data_path, f))]
freq_to_test = [('broadband', [4, 25]), ('theta', [5, 8]), ('alpha', [9, 13]), ('beta', [14, 24])]

## do knn decoding (This takes some time!)...
data_dict = {}
for idx, subject in enumerate(all_files):
    f_name = all_files[idx]

    # load data...
    data = read_mat(join(data_path, f_name))

    X = data['powspctrm']
    y = data['trlinfo']
    freqs = data['freqs']

    # scale data...
    X = StandardScaler().fit_transform(X)

    # create a new KNN model...
    knn_cv = KNeighborsClassifier()

    # define cross-validation...
    cv = StratifiedKFold(n_splits=2, shuffle=True, random_state=1770)

    # define max amount of neighbors...
    max_neighbors = int(X.shape[0] / 10)

    # create a dict of all values we want to test as neighbors
    param_grid = {'n_neighbors': np.arange(1, max_neighbors, 2)}

    # loop over FOIs...
    freq_dict = {}
    for idx_freq, foi in enumerate(freq_to_test):
        # define FOI boundaries...
        cur_band = (freqs >= foi[1][0]) * (freqs < foi[1][1])
        band_idx = np.where(cur_band)
        X_band = X[:, band_idx[0]]

        # use Gridsearch to test all values for n_neighbors...
        knn_gscv = GridSearchCV(knn_cv, param_grid, cv=cv)

        # fit model to the data...
        knn_gscv.fit(X_band, y)

        # create new knn model with best_params for random permutation test...
        knn_best_param = KNeighborsClassifier(n_neighbors=knn_gscv.best_params_['n_neighbors'])

        # random permutation test...
        score, permutation_scores, pvalue = permutation_test_score(
            knn_best_param, X_band, y, scoring='accuracy', cv=cv, n_permutations=1000, random_state=1770)

        # calculate chance value from permutation scores...
        perm_mean = np.mean(permutation_scores, dtype=np.float64)
        print('{}: acc = {}, p-value = {}, chance = {}'.format(foi[0], score, pvalue, perm_mean))

        # store in dict...
        freq_dict[foi[0]] = {'observed': [score, pvalue],
                                              'chance': [perm_mean, float('NaN')],
                                              }

    # create a nested dict with all data...
    data_dict[idx] = {subject: freq_dict}
    print('Subject {} done...!'.format(idx + 1))

## create dataframe...
df_data_all = pd.DataFrame([(k1, k2, k3, v3[0], v3[1])
                            for k, v in data_dict.items()
                            for k1, v1 in v.items()
                            for k2, v2 in v1.items()
                            for k3, v3 in v2.items()],
                           columns=['sub_id', 'foi', 'type', 'accuracy', 'p_value'])

## plot Fig. 2C...
# sort subject ids
max_accs = df_data_all.query("foi=='broadband' & type=='observed'").sort_values("accuracy", ascending=False)["sub_id"]

# decoding plot + means/ci...
sns.set_theme(style='white')

g = sns.FacetGrid(df_data_all, col='foi')
g.map_dataframe(sns.pointplot, x='type', y='accuracy', hue='sub_id', scale=1, dodge=False,
                palette="viridis", hue_order=max_accs)

g.map_dataframe(sns.pointplot, x='type', y='accuracy', join=False, dodge=True, markers='o', color='black',
                scale=1.5, errwidth=5, ci=95, jitter=.5)
g.set(ylim=(None, .60))
plt.legend(loc='center right', bbox_to_anchor=(1.25, 0.5), ncol=1)
plt.show()

## Descriptives...
grouped_data = df_data_all.groupby(['foi', 'type'])
df_grouped = grouped_data['accuracy'].mean().reset_index()
print(df_grouped)

## prepare statistical tests...
df_foi_broadband = df_data_all[(df_data_all.foi == 'broadband')]
df_foi_theta = df_data_all[(df_data_all.foi == 'theta')]
df_foi_alpha = df_data_all[(df_data_all.foi == 'alpha')]
df_foi_beta = df_data_all[(df_data_all.foi == 'beta')]

## ttest broadband...
stats = pg.ttest(df_foi_broadband.accuracy[df_foi_broadband.type == 'observed'],
                 df_foi_broadband.accuracy[df_foi_broadband.type == 'chance'], paired=True, alternative='greater')
print(stats)

## One-way rmANOVA...
aov = pg.rm_anova(data=df_data_all, dv='accuracy', within=['foi', 'type'], subject='sub_id', detailed=False)
print(aov.round(3))

## post-hoc...
stats_theta = pg.ttest(df_foi_theta.accuracy[df_foi_theta.type == 'observed'],
                       df_foi_theta.accuracy[df_foi_theta.type == 'chance'],
                       paired=True, alternative='greater')
print(stats_theta)
stats_alpha = pg.ttest(df_foi_alpha.accuracy[df_foi_alpha.type == 'observed'],
                       df_foi_alpha.accuracy[df_foi_alpha.type == 'chance'],
                       paired=True, alternative='greater')
print(stats_alpha)
stats_beta = pg.ttest(df_foi_beta.accuracy[df_foi_beta.type == 'observed'],
                      df_foi_beta.accuracy[df_foi_beta.type == 'chance'],
                       paired=True, alternative='greater')
print(stats_beta)
