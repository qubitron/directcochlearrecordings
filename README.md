# DirectCochlearRecordings

Data/Code for the manuscript entitled "Direct cochlear recordings in humans show a theta rhythmic modulation of the auditory nerve by selective attention" 

Note that the Matlab analysis script requires a running version of the 'obob_ownft' (https://gitlab.com/obob/obob_ownft), a collection of toolboxes and functions used to analyze MEG and EEG data
